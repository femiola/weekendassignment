import UIKit


class Node <T>{
    var myvalue: T
    var nextPointer: Node<T>?
    weak var perviousPointer: Node<T>?
    
    init(anyTypevalue: T) {
        self.myvalue = anyTypevalue
    }
}

//Keeping track of where the list begins and ends
class DoublyLinkedList{
    fileprivate var theNodehead: Node<Any>?
    private var theNodetail: Node<Any>?
    
    public var listIsEmpty: Bool{
        return theNodehead == nil
    }
    
    public var first: Node<Any>?{
        return theNodehead
    }
    public var last:  Node<Any>?{
        return theNodetail
    }
    var checkLenght: Int{
        get{
            var count = 0
            var currentCount = self.theNodehead
            while currentCount != nil{
                count += 1
                currentCount = currentCount?.nextPointer
            }
            return count
        }
    }
    
    //A function that adds the specified item to the end of the list
    public func add(value: Any){
        let myNewNode = Node(anyTypevalue: value)
        
        if let tailNode = theNodetail{
            myNewNode.perviousPointer = tailNode
            tailNode.nextPointer = myNewNode
        }
        else{
            theNodehead = myNewNode
        }
        theNodetail = myNewNode
    }
    
    //A function that accepts an index and returns the node at the specified index
    public func nodeAt(index: Int) -> Node<Any>? {
        if index >= 0 {
            var theNode = theNodehead
            var i = index
            while theNode != nil {
                if i == 0 { return theNode }
                i -= 1
                theNode = theNode!.nextPointer
            }
        }
        return nil
    }
    
    //A function that accepts a node, removes the specified node and returns the
    //value of the node
    public func remove(node: Node<Any>) -> Any {
        let previous = node.perviousPointer
        let next = node.nextPointer
        
        if let previous = previous {
            previous.nextPointer = next
        } else {
            theNodehead = next
        }
        next?.perviousPointer = previous
        
        if next == nil {
            theNodetail = previous
        }
        node.perviousPointer = nil
        node.nextPointer = nil
        return node.myvalue
    }
}

class Stack {
    
    let myItems = DoublyLinkedList()
    
    var size: Int {
        get { return myItems.checkLenght }
    }
    var peek: Any? {
        get { return myItems.last! }
    }
    func push(_ v: Any) {
        myItems.add(value: v)
    }
    
    func pop() -> Any? {
        return myItems.remove(node: myItems.last!)
    }
    
}


extension DoublyLinkedList: CustomStringConvertible {
    public var description: String {
        var myText = "H-"
        
        var myNodeList = theNodehead
        while myNodeList != nil {
            myText += "[\(myNodeList!.myvalue)]"
            
            myNodeList = myNodeList!.nextPointer
            if myNodeList != nil { myText += "-" }
        }
        return myText + "-T"
    }
}
//Test for doubleLinked
//let dogBreeds = DoublyLinkedList()
//dogBreeds.add(value: "Labrador")
//dogBreeds.add(value: "Bulldog")
//dogBreeds.add(value: "Beagle")
//dogBreeds.add(value: "Husky")
//
//
//print(dogBreeds)
//
//
//print(dogBreeds.nodeAt(index: 2)?.myvalue as Any)
//
//print(dogBreeds.remove(node: Node<Any>(anyTypevalue: "Beagle")))
//

extension Node: CustomStringConvertible {
    public var description: String {
        let  textNode = "[\(myvalue)]"
        
        return  textNode
    }
}
//Test for Node
//let number = Node(anyTypevalue: 2)
//print(number)


extension Stack : CustomStringConvertible{
    var description: String {
        var myStackText = "["
        var myStackList = myItems.theNodehead
        while myStackList != nil {
            myStackText += "\(String(describing: myStackList!.myvalue))"
            myStackList = myStackList!.nextPointer
            if myStackList != nil { myStackText += ", " }
        }
        return myStackText + "]"
    }
}

//Test for stack
let number = Stack()

print(number.push(1))
print(number.push(2))
print(number.push(3))
number.push(4)
print(number.peek as Any)
print(number.size)
print(number.pop() as Any)
print(number.pop() as Any)
print(number.size)
print(number)
















